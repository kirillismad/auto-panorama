from random import choice

from django.conf import settings
from django.core.files import File

from utils.tests.factories.primitive import PrimitiveFactory
from utils.tests.patches import patch_storage
from watching.choices import RoleType, QualityType
# from watching.models import Genre, TvSeries, Season, Episode, Film, Person, Participant, Video
from watching.models import *


class WatchingFactory:
    def __init__(self, primitive_factory: PrimitiveFactory):
        self.primitive_factory = primitive_factory

    def get_genre(self, **kwargs) -> Genre:
        kwargs.setdefault('title', self.primitive_factory.get_title())
        return Genre.objects.create(**kwargs)

    def set_participants(self, participle, n=2):
        for _ in range(n):
            self.get_participant(participle=participle)

    def set_videos(self, viewable, n=2):
        for _ in range(n):
            self.get_video(viewable=viewable)

    def set_genre_set(self, genred, n=2):
        genres = [self.get_genre() for _ in range(n)]
        genred.genre_set.set(genres)

    def set_imaged_fields(self, kwargs):
        if 'poster' not in kwargs:
            kwargs['poster'] = self.primitive_factory.get_image_file()
        if 'background' not in kwargs:
            kwargs['background'] = self.primitive_factory.get_image_file()

    def get_preview(self):
        return self.primitive_factory.get_image_file()

    @patch_storage
    def get_film(self, is_set_participants=False, is_set_videos=False, is_set_genres=False, **kwargs) -> Film:
        kwargs.setdefault('title', self.primitive_factory.get_title())
        kwargs.setdefault('description', self.primitive_factory.get_text())
        kwargs.setdefault('duration', self.primitive_factory.get_duration())
        kwargs.setdefault('release_date', self.primitive_factory.get_date())
        if 'preview' not in kwargs:
            kwargs['preview'] = self.primitive_factory.get_image_file()

        self.set_imaged_fields(kwargs)

        film = Film.objects.create(**kwargs)
        if is_set_participants:
            self.set_participants(film)

        if is_set_videos:
            self.set_videos(film)

        if is_set_genres:
            self.set_genre_set(film)

        return film

    def get_tv_series(self, is_set_participants=False, **kwargs) -> TvSeries:
        kwargs.setdefault('title', self.primitive_factory.get_title())
        kwargs.setdefault('description', self.primitive_factory.get_text())
        kwargs.setdefault('date_start', self.primitive_factory.get_date())

        tv_series = TvSeries.objects.create(**kwargs)
        if is_set_participants:
            self.set_participants(tv_series)

        return tv_series

    def get_season(self, is_set_participants=False, **kwargs) -> Season:
        kwargs.setdefault('title', self.primitive_factory.get_title())
        kwargs.setdefault('description', self.primitive_factory.get_text())
        kwargs.setdefault('number', self.primitive_factory.get_int())
        if 'tv_series' not in kwargs:
            kwargs['tv_series'] = self.get_tv_series()

        season = Season.objects.create(**kwargs)
        if is_set_participants:
            self.set_participants(season)

        return season

    @patch_storage
    def get_episode(self, is_set_participants=False, is_set_videos=False, **kwargs) -> Episode:
        kwargs.setdefault('title', self.primitive_factory.get_title())
        kwargs.setdefault('description', self.primitive_factory.get_text())
        kwargs.setdefault('number', self.primitive_factory.get_int())
        kwargs.setdefault('duration', self.primitive_factory.get_duration())
        kwargs.setdefault('release_date', self.primitive_factory.get_date())

        if 'preview' not in kwargs:
            kwargs['preview'] = self.primitive_factory.get_image_file()

        if 'season' not in kwargs:
            kwargs['season'] = self.get_season()

        episode = Episode.objects.create(**kwargs)
        if is_set_participants:
            self.set_participants(episode)

        if is_set_videos:
            self.set_videos(episode)

        return episode

    def get_person(self, **kwargs) -> Person:
        kwargs.setdefault('first_name', self.primitive_factory.get_title())
        kwargs.setdefault('last_name', self.primitive_factory.get_title())
        return Person.objects.create(**kwargs)

    def get_participant(self, **kwargs) -> Participant:
        kwargs.setdefault('role', choice(list(RoleType)))
        if 'participle' not in kwargs:
            kwargs['participle'] = self.get_film()

        if 'person' not in kwargs:
            kwargs['person'] = self.get_person()

        return Participant.objects.create(**kwargs)

    def get_video_file(self) -> File:
        name = 'video.mp4'
        path = settings.BASE_DIR.joinpath('utils', 'tests', 'samples', name)
        with open(path, 'rb') as f:
            return File(f, name)

    def get_viewable(self):
        method = choice([self.get_episode, self.get_film])
        return method()

    @patch_storage
    def get_video(self, **kwargs) -> Video:
        kwargs.setdefault('quality', choice(list(QualityType)))
        if 'file' not in kwargs:
            kwargs['file'] = self.get_video_file()
        if 'viewable' not in kwargs:
            kwargs['viewable'] = self.get_viewable()

        return Video.objects.create(**kwargs)
