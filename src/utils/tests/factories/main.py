from random import choice

from main.models import User, Profile


class MainFactory:
    def __init__(self, primitive_factory):
        self.primitive_factory = primitive_factory

    def _get_email(self) -> str:
        name = self.primitive_factory.get_string()
        service = choice(['list', 'mail', 'inbox', 'bk'])
        return f'{name}@{service}.ru'

    def get_password(self):
        return User.objects.make_random_password()

    def get_email(self):
        exists_email = set(User.objects.values_list('email', flat=True))
        while True:
            email = self._get_email()
            if email not in exists_email:
                return email

    def get_user(self, **kwargs) -> User:
        kwargs.setdefault('email', self.get_email())
        kwargs.setdefault('password', self.get_password())

        return User.objects.create_user(**kwargs)

    def get_profile(self, **kwargs) -> Profile:
        if 'user' not in kwargs:
            kwargs['user'] = self.get_user()

        return Profile.objects.create(**kwargs)
