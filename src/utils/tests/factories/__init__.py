from .primitive import PrimitiveFactory
from .main import MainFactory
from .watching import WatchingFactory


def get_primitive_factory():
    return PrimitiveFactory()


def get_main_factory(p_factory=None):
    if p_factory is None:
        p_factory = get_primitive_factory()
    return MainFactory(p_factory)


def get_watching_factory(p_factory):
    if p_factory is not None:
        p_factory = get_primitive_factory()
    return WatchingFactory(p_factory)
