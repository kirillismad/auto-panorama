from django.db import models
from django.utils.translation import gettext_lazy as _
from utils.upload_to import UploadToFactory
from utils.choices import MaturityRating
from django.apps import apps


class TitledModelMixin(models.Model):
    title = models.CharField(_('title'), max_length=32)

    class Meta:
        abstract = True


class DescribedModelMixin(models.Model):
    description = models.TextField(_('description'))

    class Meta:
        abstract = True


class NumberedModelMixin(models.Model):
    number = models.PositiveSmallIntegerField(_('number'))

    class Meta:
        abstract = True


class CreatedAtModelMixin(models.Model):
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)

    class Meta:
        abstract = True


class ReleaseDateModelMixin(models.Model):
    release_date = models.DateField(_('release date'))

    class Meta:
        abstract = True


class MaturityRatingModelMixin(models.Model):
    maturity = models.CharField(max_length=4, choices=MaturityRating.choices())

    class Meta:
        abstract = True


class PosterUploadToFactory(UploadToFactory):
    def get_path(self, instance):
        if isinstance(instance, apps.get_model('watching', 'Film')):
            return 'watching/film/poster'
        elif isinstance(instance, apps.get_model('watching', 'TvSeries')):
            return 'watching/tv_series/poster'

        raise ValueError(f'Invalid model {instance.__class__.__name__}')


class BackgroundUploadToFactory(UploadToFactory):
    def get_path(self, instance):
        if isinstance(instance, apps.get_model('watching', 'Film')):
            return 'watching/film/background'
        elif isinstance(instance, apps.get_model('watching', 'TvSeries')):
            return 'watching/tv_series/background'

        raise ValueError(f'Invalid model {instance.__class__.__name__}')


class ImagedModelMixin(models.Model):
    poster = models.ImageField(_('poster'), upload_to=PosterUploadToFactory())
    background = models.ImageField(_('background'), upload_to=BackgroundUploadToFactory())

    class Meta:
        abstract = True
