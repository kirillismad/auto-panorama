from rest_framework import serializers
from django.utils.translation import gettext_lazy as _

# PSQL_SMALL_MAX_INT = 32767
#
#
# class TitleSerializerMixin(serializers.Serializer):
#     title = serializers.CharField(max_length=32, label=_('title'))
#
#
# class DescribedSerializerMixin(serializers.Serializer):
#     description = serializers.CharField(label=_('description'))
#
#
# class NumberedSerializerMixin(serializers.Serializer):
#     number = serializers.IntegerField(min_value=0, max_value=PSQL_SMALL_MAX_INT, label=_('number'))
#
#
# class CreatedAtSerializerMixin(serializers.Serializer):
#     created_at = serializers.DateTimeField(label=_('created at'))
