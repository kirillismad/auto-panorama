from django.contrib.admin import ModelAdmin


class BaseModelAdmin(ModelAdmin):
    def get_list_display_links(self, request, list_display):
        return list_display
