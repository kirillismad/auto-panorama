from enum import Enum


class ChoiceEnum(str, Enum):
    @classmethod
    def choices(cls):
        raise NotImplementedError

    def __str__(self):
        return self.value
