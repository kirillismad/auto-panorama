from utils.choice_enum import ChoiceEnum
from django.utils.translation import gettext_lazy as _


class RoleType(ChoiceEnum):
    ACTOR = 'AC'
    DIRECTOR = 'DR'
    OPERATOR = 'OP'
    PRODUCER = 'PR'

    @classmethod
    def choices(cls):
        return (
            (cls.ACTOR, _('Actor')),
            (cls.DIRECTOR, _('Director')),
            (cls.OPERATOR, _('Operator')),
            (cls.PRODUCER, _('Producer')),
        )


class QualityType(ChoiceEnum):
    UHD = 'UHD'
    FHD = 'FHD'
    HD = 'HD'
    ND = 'ND'
    LD = 'LD'

    @classmethod
    def choices(cls):
        return (
            (cls.UHD, _('Ultra High-Definition (4k)')),
            (cls.FHD, _('Full High-Definition (2k)')),
            (cls.HD, _('High-Definition (720p)')),
            (cls.ND, _('Normal-Definition (480p)')),
            (cls.LD, _('Low-Definition (360p)')),
        )
