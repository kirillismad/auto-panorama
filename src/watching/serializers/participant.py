from rest_framework import serializers

from watching.models import Participant
from watching.serializers import person

__all__ = [
    'ParticipantSerializer',
]


class ParticipantSerializer(serializers.ModelSerializer):
    person = person.PersonSerializer(read_only=True)

    class Meta:
        model = Participant
        fields = ('role', 'person')
        read_only_fields = ('role',)
