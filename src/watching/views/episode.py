from rest_framework.generics import ListAPIView, RetrieveAPIView

from utils.mixins import FilterQuerysetMixin
from watching import serializers
from watching.models import Episode

__all__ = [
    'EpisodeView',
    'EpisodeDetailView',
]


class EpisodeView(FilterQuerysetMixin, ListAPIView):
    queryset = Episode.objects.all()
    serializer_class = serializers.EpisodeSerializer

    filter_kwargs = {'season__tv_series_id': 'id', 'season_id': 'season_id'}


class EpisodeDetailView(FilterQuerysetMixin, RetrieveAPIView):
    queryset = Episode.objects.all()
    serializer_class = serializers.EpisodeDetailSerializer

    filter_kwargs = {'season__tv_series_id': 'id', 'season_id': 'season_id'}

    lookup_url_kwarg = 'episode_id'
