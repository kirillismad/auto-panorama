from django.urls import path
from main import views

app_name = 'main'

urlpatterns = [
    path('sign-up/', views.SignUpView.as_view(), name='sign_up'),
    path('sign-up/<str:uid>/confirm/', views.SignUpConfirmView.as_view(), name='sign_up_confirm'),
    path('sign-in/', views.SignInView.as_view(), name='sign_in'),
    path('restore-password/', views.PasswordRestoreView.as_view(), name='restore_password'),
    path('restore-password/<uid>/confirm/', views.PasswordRestoreConfirmView.as_view(), name='restore_password_confirm')
]