from django.conf import settings

from main.tasks import send_email_task


class BaseEmailManager:
    subject = None
    from_email = settings.EMAIL_FROM

    def send_email(self, email, link):
        assert self.subject is not None
        body = self.get_body(link)
        send_email_task.delay(self.subject, body, self.from_email, [email])

    def get_body(self, link):
        return link


class SignUpEmailManager(BaseEmailManager):
    subject = 'Confirm account'


class RestorePasswordEmailManager(BaseEmailManager):
    subject = 'Reset password'
