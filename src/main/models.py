from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import CASCADE
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from main.managers import UserManager
from utils.models import CreatedAtModelMixin


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email'), unique=True)

    is_active = models.BooleanField(_('active status'), default=True)
    is_staff = models.BooleanField(_('staff status'))
    is_confirm = models.BooleanField(_('confirm status'), default=False)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.email


class Profile(CreatedAtModelMixin):
    user = models.OneToOneField(User, CASCADE, primary_key=True, verbose_name=_('user'))
    rated_set = models.ManyToManyField('watching.Rated', related_name='profiles', through='Estimate')

    @cached_property
    def email(self):
        return self.user.email


class Estimate(models.Model):
    profile = models.ForeignKey('Profile', CASCADE, related_name='estimates')
    rated = models.ForeignKey('watching.Rated', CASCADE, related_name='estimates')
    rate = models.PositiveSmallIntegerField(validators=[MinValueValidator(0), MaxValueValidator(10)])
