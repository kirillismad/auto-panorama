from django.contrib.admin import site, autodiscover
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token

autodiscover()
site.unregister([Group, Token])
