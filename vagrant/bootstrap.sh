#!/usr/bin/env bash
YQ="-y -q"

export DEBIAN_FRONTEND=noninteractive
apt update -y -qq

# prerequirements
apt install $YQ software-properties-common python-software-properties build-essential libssl-dev libffi-dev wget ca-certificates

#python3.7
apt update $YQ
add-apt-repository ppa:deadsnakes/ppa
apt install $YQ python3.7

# alias
ln -s /usr/bin/python3.7 /usr/local/bin/python

# pip
apt update $YQ
apt install $YQ python3-pip

# python environment
apt install $YQ python3-dev python3.7-dev libpq-dev

# venv
apt install $YQ python3.7-venv

# postgresql
su -c "printf 'deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main' >> /etc/apt/sources.list.d/pgdg.list"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
sudo apt update $YQ
sudo apt install $YQ postgresql postgresql-contrib

while IFS='' read -r line || [[ -n "$line" ]]; do
    su postgres -c "psql -c \"$line\" "
done < /vagrant/vagrant/init_db.sql

printf "localhost:*:auto_panorama:auto_panorama_user:password123" > /root/.pgpass
chmod 0600 /root/.pgpass

# psql -U auto_panorama_user -h localhost auto_panorama -w < /vagrant/psql/dump.sql

# memcached
apt install $YQ memcached

# gettext
apt install $YQ gettext

# rabbitmq
apt install $YQ rabbitmq-server
sudo rabbitmqctl add_user auto_panorama_user password123
sudo rabbitmqctl set_user_tags auto_panorama_user administrator
sudo rabbitmqctl set_permissions -p / auto_panorama_user ".*" ".*" ".*"
sudo systemctl restart rabbitmq-server

# ffmpeg
apt update $YQ
apt install $YQ ffmpeg

# install dependencies
python -m venv /home/vagrant/v_env
chown -R vagrant:vagrant /home/vagrant/v_env

ACTIVATE_ENV="/home/vagrant/v_env/bin/activate"
printf "export DJANGO_SETTINGS_MODULE=root.settings.dev" >> $ACTIVATE_ENV

source $ACTIVATE_ENV
pip install --upgrade pip
pip install -r /vagrant/src/requirements.txt
python /vagrant/src/manage.py migrate
deactivate

chown -R vagrant:vagrant /home/vagrant/v_env


