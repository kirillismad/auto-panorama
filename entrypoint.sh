#!/bin/bash

# Prepare log files and start outputting logs to stdout
mkdir ./logs
touch ./logs/gunicorn.log

python3 manage.py migrate
python3 manage.py collectstatic --noinput

gunicorn src.wsgi:application --name auto_panorama --bind 0.0.0.0:8000 --workers 3 --worker-class=gevent --timeout 120 --log-level=info --log-file=./logs/gunicorn.log --access-logfile '-'
